<?php declare(strict_types=1);

/*
 * This file is part of the anastaszor/shell-ci-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Test\Test\ContinuousIntegration;

/**
 * @internal
 * @covers \Test\Test\ContinuousIntegration
 * @small
 */
class ContinuousIntegrationTest extends TestCase
{
	
	public function testEverythingIsOk() : void
	{
		$ci = new ContinuousIntegration();
		
		$this->assertEquals('OK', $ci->run());
	}
	
}
