<?php declare(strict_types=1);

/*
 * This file is part of the anastaszor/shell-ci-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Test\Test\ContinuousIntegrationInterface;

/**
 * @internal
 * @coversNothing
 * @small
 */
class ContinuousIntegrationInterfaceTest extends TestCase
{
	
	public function testEverythingIsOk() : void
	{
		$mock = $this->getMockForAbstractClass(ContinuousIntegrationInterface::class);
		
		$mock->expects($this->any())
			->method('run')
			->willReturn('OK')
		;
		
		$this->assertEquals('OK', $mock->run());
	}
	
}
