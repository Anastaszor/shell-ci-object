#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR/..")

REPODIR=$(realpath "$CURRDIR/..")
TODAY=$(date +%Y-%m-%d)

# loop on all repositories that are found next this one
for REPO in $(find "$REPODIR" -maxdepth 1 -type d | sort -r)
do
	# ignore repositories that are not code repositories
	REPONAME=$(basename "$REPO")
	
	if [[ "$REPONAME" == $(basename "$REPODIR") ]]
	then
		continue
	fi
	
	# ignore all directories that do not start with :
	[ ${REPONAME:0:3} != "php" ] &&
	[ ${REPONAME:0:3} != "yii" ] &&
	[ ${REPONAME:0:3} != "mag" ] &&
	[ ${REPONAME:0:3} != "mtg" ] &&
	[ ${REPONAME:0:4} != "poly" ] && continue
	
	echo "Processing $REPO"
	cd "$REPO"
	echo "cd $REPO"
	git reset --hard HEAD
	git clean -f
	
done
