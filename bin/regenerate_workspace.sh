#!/bin/bash
set -eu
IFS=$'\n\t'

# {{{ FUNCTIONS

# Writes into the console the date and a message
# @var $1 string the message to write
log() {
	echo -e "[$(date '+%Y-%m-%d %H:%M:%S')] $1"
}

# Writes into the console the given string in WHITE
# @var $1 string the message to write
log_info() {
	log "[  INFO   ] $1"
}

# Writes into the console the given string in RED
# @var $1 string the message to write
log_error() {
	log "[  \033[31mERROR\033[0m  ] $1"
}

# Writes into the console the given string in GREEN
# @var $1 string the message to write
log_success() {
	log "[ \033[32mSUCCESS\033[0m ] $1"
}

# Writes into the console the given string in YELLOW
log_warning() {
	log "[ \033[33mWARNING\033[0m ] $1"
}

# Regenerates the whole suite and its associated repositories
# @var $1 string the hostname of the suite
# @var $2 string the namespace of the repo suite
# @var $3 string the name of the repo suite
# @var $4 bool whether this repo is for interfaces
renegerate_suite() {

    SUITEHN="$1"
    SUITENS="$2"
    SUITERP="$3"
    ISINTFC=$4

    PHPEXTENDEDSUITEDIR="$(realpath "$SUITEDIR/$SUITERP")"

    # Check if the PHP Suite directory exists
    if [[ ! -d "$PHPEXTENDEDSUITEDIR" ]]; then
        # Download the git repository to this directory
        git clone "git@${SUITEHN}:${SUITENS}/${SUITERP}.git" "$PHPEXTENDEDSUITEDIR"
        STATUS=$?
        if [[ $STATUS -ne 0 ]]; then
            log_error "Failed to download the PHP Suite directory."
            exit 1
        fi
    else
        log_info "$PHPEXTENDEDSUITEDIR already exists"
    fi

    PHPEXTENDEDSUITECOMPOSERJSON="$(realpath "$PHPEXTENDEDSUITEDIR/composer.json")"
    # Check if the composer.json file exists
    if [[ ! -f "$PHPEXTENDEDSUITECOMPOSERJSON" ]]; then
        log_error "The composer.json file does not exist in the PHP Suite directory."
        exit 1
    fi

    if [[ $ISINTFC == true ]]
    then
        log_info "Collecting all interfaces repositories from $PHPEXTENDEDSUITECOMPOSERJSON"
        # Grep all the directories that have "interface" in their name present in the require of this composer.json file
        REPONAMES=$(cat "$PHPEXTENDEDSUITECOMPOSERJSON" | jq '.require' | grep -v '"php"' | grep "interface" | sed 's/"//g' | sed 's/:.*$//g' | sed 's/ //g')
    else
        log_info "Collecting all implementation repositories from $PHPEXTENDEDSUITECOMPOSERJSON"
        # Grep all the directories that dont have "interface" in their name present in the require of this composer.json file
        REPONAMES=$(cat "$PHPEXTENDEDSUITECOMPOSERJSON" | jq '.require' | grep -v '"php"' | grep "/" | grep -v "interface" | sed 's/"//g' | sed 's/:.*$//g' | sed 's/ //g')
    fi

    # For each reponame download it if its not there
    for REPONAME in $REPONAMES; do
        REPODIR="$WSDIR/$REPONAME"
        # take only the part after the / from the REPONAME
        REPOTARGET="${WSDIR}/${REPONAME##*/}"
        if [[ ! -d "$REPOTARGET" ]]; then
            log_info "Downloading repository $REPONAME..."
            git clone "git@${SUITEHN}:$REPONAME.git" "$REPOTARGET"
            STATUS=$?
            if [[ $STATUS -ne 0 ]]; then
                log_error "Failed to download the repository $REPONAME."
                continue
            fi
            log_success "Repository $REPONAME downloaded successfully."
        else
            log_info "$REPODIR already exists"
        fi
    done

}

# }}} FUNCTIONS


CURRDIR="$(dirname "$0")"
CURRDIR="$(realpath "$CURRDIR/..")"
CURRDIRNAME="$(basename "$CURRDIR")"
WSDIR="$(realpath "$CURRDIR/..")"
SUITEDIR="$(realpath "$WSDIR/../php-suite")"

# Check if the necessary directories exist
if [[ ! -d "$WSDIR" ||! -d "$SUITEDIR" ]]; then
    log_error "The workspace or the PHP Suite directory does not exist."
    exit 1
fi

if [[ "$CURRDIRNAME" == "shell-ci-interface" ]]
then
    ISINTFC=true
else
    ISINTFC=false
fi

renegerate_suite "gitlab.com" "php-extended" "php-extended-all-suite" $ISINTFC
renegerate_suite "gitlab.com" "php-mtg" "php-mtg-all-suite" $ISINTFC
renegerate_suite "gitlab.com" "yii2-extended" "yii2-extended-all-suite" $ISINTFC
renegerate_suite "ubser-gitlab.local" "php-mtg" "local-php-mtg-all-suite" $ISINTFC
