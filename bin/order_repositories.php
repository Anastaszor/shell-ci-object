<?php

$rootDir = dirname(__DIR__, 2);

$scan = scandir($rootDir);
if($scan === false)
{
    throw new RuntimeException('Failed to scan directory at '.$rootDir);
}

$stack = [];

foreach(array_reverse($scan) as $dir)
{
    if($dir === '.' || $dir === '..')
    {
        continue;
    }

    $fullPath = $rootDir.'/'.$dir;
    if(!is_dir($fullPath))
    {
        continue;
    }

    $composerPath = $fullPath.'/composer.json';
    if(!is_file($composerPath))
    {
        throw new RuntimeException('Failed to find expected composer.json at '.$composerPath);
    }

    $composerContents = @file_get_contents($composerPath);
    if($composerContents === false)
    {
        throw new RuntimeException('Failed to read composer.json at '.$composerPath);
    }

    $composerJson = json_decode($composerContents, true);
    if(!is_array($composerJson))
    {
        throw new RuntimeException('Invalid composer.json format at '.$composerPath);
    }
    
    if(!isset($composerJson['name']))
    {
        throw new RuntimeException('Failed to find name in composer.json at '.$composerPath);
    }

    $name = $composerJson['name'];

    if(!str_starts_with($name, 'php') && !str_starts_with($name, 'polyfill') && !str_starts_with($name, 'yii'))
    {
        continue;
    }

    if(!isset($composerJson['require']))
    {
        throw new RuntimeException('Failed to find require in composer.json at '.$composerPath);
    }
    if(!is_array($composerJson['require']))
    {
        throw new RuntimeException('Invalid require in composer.json format at '.$composerPath);
    }

    $require = array_keys($composerJson['require']);

    if(isset($composerJson['require-dev']))
    {
        if(!is_array($composerJson['require-dev']))
        {
            throw new RuntimeException('Invalid require-dev in composer.json format at '.$composerPath);
        }

        $require = array_merge($require, array_keys($composerJson['require-dev']));
    }

    $require = array_filter($require, function(string $entry) : bool
    {
        return str_starts_with($entry, 'php-extended/') && strpos($entry, 'interface') === false;
    });

    $stack[] = ['name' => $name, 'path' => $fullPath, 'require' => $require];
}

$finalOrder = [];

while(count($stack) > 0)
{
    $lib = array_shift($stack);

    $allFound = true;

    foreach($lib['require'] as $requiredLib)
    {
        if(!isset($finalOrder[$requiredLib]))
        {
            $allFound = false;
            break;
        }
    }

    if(!$allFound)
    {
        $stack[] = $lib;
        continue;
    }

    $finalOrder[$lib['name']] = $lib['path'];
}

echo implode("\n", $finalOrder)."\n";
