<?php declare(strict_types=1);

/*
 * This file is part of the anastaszor/shell-ci-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Test\Test;

/**
 * Tests the ContinuousIntegrationInterface class.
 *
 * @author Anastaszor
 */
interface ContinuousIntegrationInterface
{
	
	/**
	 * Tests the CI.
	 */
	public function run() : string;
	
}
