# test-php-ci

A test project to evaluate .gitlab-ci capabilities

![coverage](https://gitlab.com/Anastaszor/test-php-ci/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/Anastaszor/test-php-ci/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install anastaszor/shell-ci-object ^8`
